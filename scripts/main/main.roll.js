;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.roll = {
    settings: {
      autoinit: false,
      speed: 100
    },

    to: function ($target) {
      var that = this;

      if (!$target) {
        $target = $('body');
      }

      $('html, body').animate({
        scrollTop: $target.offset().top
      }, that.settings.speed);
    }
  };
}(jQuery, this, this.document));
