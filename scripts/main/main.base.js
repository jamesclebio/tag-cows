;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.base = {
    settings: {
      autoinit: false,
      container: '.default-content > .container'
    },

    views: {
      edit: 'views/edit.html',
      list: 'views/list.html'
    },

    api: {
      url: 'http://recrutamento.taginterativa.com.br/api/v1/',
      accessToken: '4073b7d950'
    }
  };
} (jQuery, this, this.document));
