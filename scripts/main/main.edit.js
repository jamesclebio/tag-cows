;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.edit = {
    settings: {
      autoinit: false
    },

    init: function (update) {
      this.handler(update);
      this.builder(update);
    },

    handler: function (update) {
      var that = this;

      $(main.base.settings.container).on({
        submit: function(event) {
          var $fields = that.formFields();
          var fields = {};

          for (var i in $fields) {
            fields[i] = $fields[i].val();
          }

          if (that.formValidate($fields)) {
            if (update) {
              that.formSubmit(update, fields);
            } else {
              that.formSubmit(false, fields);
            }
          }

          event.preventDefault();
        }
      }, 'form');
    },

    builder: function (update) {
      var that = this;

      $.ajax({
        url: main.base.views.edit,

        beforeSend: function () {
          main.loading.on();
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          $(main.base.settings.container).html(data);

          if (update) {
            that.formFill(update);
          } else {
            main.loading.off();
          }

          $(':input:first').focus();

          if (window.location.hash === '#/home') {
            main.roll.to();
          } else {
            main.roll.to($(main.base.settings.container));
          }
        }
      });
    },

    formSubmit: function (update, fields) {
      var method = 'POST';
      var url = main.base.api.url + 'cows';

      if (update) {
        method = 'PUT';
        url += '/' + update;
      }

      $.ajax({
        method: method,
        url: url,
        contentType: 'application/json',
        headers: {
          'access-token': main.base.api.accessToken
        },
        data: JSON.stringify(fields),

        beforeSend: function() {
          main.loading.on();
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          if (data.error || data.errors) {
            this.error();
          } else {
            if (update) {
              main.alert.builder('success', 'Vaquinha atualizada com sucesso!');
            } else {
              main.alert.builder('success', 'Vaquinha cadastrada com sucesso!');
            }
            window.location.hash = '/list';
          }
        },

        timeout: 10000
      });
    },

    formFields: function () {
      return {
        weight: $('[name="weight"]'),
        age: $('[name="age"]'),
        price: $('[name="price"]')
      };
    },

    formFill: function (update) {
      var that = this;

      $.ajax({
        method: 'GET',
        url: main.base.api.url + 'cows/' + update,
        contentType: 'application/json',
        headers: {
          'access-token': main.base.api.accessToken
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          var $fields = that.formFields();

          for (var i in $fields) {
            $fields[i].val(data[i]);
          }

          $fields.price.val($fields.price.val().replace('.', ','));

          main.loading.off();
        },

        timeout: 10000
      });
    },

    formValidate: function ($fields) {
      if (!/\d{1,4}/.test($fields.weight.val())) {
        main.alert.builder('danger', 'O campo peso é obrigatório...', 'Deve conter somente números, com até 4 dígitos.');
        $fields.weight.focus();
        return false;
      }

      if (!/\d{1,2}/.test($fields.age.val())) {
        main.alert.builder('danger', 'O campo idade é obrigatório...', 'Deve conter somente números, com até 2 dígitos.');
        $fields.age.focus();
        return false;
      }

      if (!/\d+(,\d{2})?$/.test($fields.price.val())) {
        main.alert.builder('danger', 'O campo preço é obrigatório...', 'Deve conter um valor em R$ (Exemplo: 875,90).');
        $fields.price.focus();
        return false;
      }

      return true;
    }
  };
} (jQuery, this, this.document));
