;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.list = {
    settings: {
      autoinit: false,
      rows: []
    },

    init: function () {
      this.handler();
      this.builder();
    },

    handler: function () {
      var that = this;
      var $container = $(main.base.settings.container);

      // Order trigger
      $container.on({
        click: function() {
          that.order($(this));
        }
      }, '[data-order]');

      // Action button (Update, Remove)
      $container.on({
        click: function(event) {
          var id = $(this).parents('tr').data('id');

          switch ($(this).data('action')) {
            case 'update':
              window.location.hash = '/edit/' + id;
              break;

            case 'remove':
              that.removeAction(id);
              break;
          }

          event.preventDefault();
        }
      }, '[data-action]');
    },

    builder: function () {
      var that = this;

      $.ajax({
        url: main.base.views.list,

        beforeSend: function () {
          main.loading.on();
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          $(main.base.settings.container).html(data);
          that.rows();
          main.roll.to();
        }
      });
    },

    rows: function (order, offset) {
      var that = this;

      offset = offset || 0;
      order = order || 'order_by[id]=DESC';

      $.ajax({
        method: 'GET',
        url: main.base.api.url + 'cows?offset=' + offset + '&' + order,
        contentType: 'application/json',
        headers: {
          'access-token': main.base.api.accessToken
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        beforeSend: function () {
          if (!offset) {
            that.settings.rows = [];
          }
        },

        success: function (data) {
          var $container = $('.table > tbody');
          var templateRow = $.templates('#templateRow');
          var recommended = 0;

          data = data || [];

          if (data.error ) {
            this.error();
          } else {
            that.settings.rows = that.settings.rows.concat(data);

            if (data.length === (offset + 20)) {
              that.rows(order, (offset + 20));
            } else {
              if (that.settings.rows.length) {
                for (var i in that.settings.rows) {
                  that.settings.rows[i].price = that.settings.rows[i].price.toFixed(2).replace('.', ',');
                  that.settings.rows[i].monthlyFood = that.monthlyFood(that.settings.rows[i].weight);
                  that.settings.rows[i].annualCost = that.annualCost(that.settings.rows[i].monthlyFood);
                  that.settings.rows[i].recommended = that.recommended(that.settings.rows[i]);

                  if (that.settings.rows[i].recommended > that.settings.rows[recommended].recommended) {
                    recommended = i;
                  }
                }

                that.settings.rows[recommended].recommendedClass = 'recommended';

                $container.html(templateRow.render(that.settings.rows));
                main.loading.off();
              } else {
                window.location.hash = '/';
              }
            }
          }
        },

        timeout: 10000
      });
    },

    monthlyFood: function (weight) {
      return (Math.round((((weight / 100) * 3) * 30) * 10) / 10).toString().replace('.', ',');
    },

    annualCost: function (monthlyFood) {
      return (((parseFloat(monthlyFood) / 30) * 365) * 0.2).toFixed(2).replace('.', ',');
    },

    recommended: function (item) {
      return (20 - item.age) / (parseFloat(item.annualCost) * parseFloat(item.price));
    },

    removeAction: function (id) {
      var that = this;

      $.ajax({
        method: 'DELETE',
        url: main.base.api.url + 'cows/' + id,
        contentType: 'application/json',
        headers: {
          'access-token': main.base.api.accessToken
        },

        beforeSend: function() {
          main.loading.on();
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          if (data.error) {
            this.error();
          } else {
            main.alert.builder('success', 'Vaquinha removida com sucesso!');
            that.rows();
          }
        },

        timeout: 10000
      });
    },

    order: function ($this) {
      var order = 'order_by' + $this.data('order');

      if (/ASC$/.test($this.data('order'))) {
        $this.data('order', $this.data('order').replace('=ASC', '=DESC'));
      } else {
        $this.data('order', $this.data('order').replace('=DESC', '=ASC'));
      }

      this.rows(order);
    }
  };
} (jQuery, this, this.document));
