;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.alert = {
    settings: {
      autoinit: false,
      main: '.alert',
      template: '<div class="alert"></div>',
      speed: 100,
      timeout: 4000
    },

    builder: function (type, heading, message) {
      var that = this;
      var $main = $(this.settings.main);

      if ($main.length) {
        $main.remove();
      }

      $main = $(this.settings.template).appendTo($('body'));

      if (type) {
        $main.addClass(type);
      }

      if (heading) {
        $main.append('<h4>' + heading + '</h4>');
      }

      if (message) {
        $main.append('<p>' + message + '</p>');
      }

      $main.fadeIn(this.settings.speed, function () {
        $(this)
          .delay(that.settings.timeout)
          .fadeOut(that.settings.speed, function () {
            $(this).remove();
          });
      });
    }
  };
}(jQuery, this, this.document));
