;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.loading = {
    settings: {
      autoinit: false,
      main: '.loading',
      template: '<div class="loading"><div class="container"><div class="spinner"></div></div></div>',
      speed: 100
    },

    builder: function () {
      var $main = $(this.settings.main);

      return ($main.length) ? $main : $(this.settings.template).appendTo($('body'));
    },

    on: function () {
      this
        .builder()
        .fadeIn(this.settings.speed);
    },

    off: function () {
      this
        .builder()
        .fadeOut(this.settings.speed);
    }
  };
}(jQuery, this, this.document));
