;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.route = {
    settings: {
      autoinit: true
    },

    init: function () {
      this.handler();
      this.view();
    },

    handler: function () {
      var that = this;

      $(window).on({
        hashchange: function() {
          that.view();
        }
      });
    },

    view: function () {
      switch (true) {
        case (/^#\/edit\/\d+/.test(window.location.hash)):
          main.edit.init(window.location.hash.replace(/^#\/edit\/(\d+)/, '$1'));
          break;

        case (/^#\/list/.test(window.location.hash)):
          main.list.init();
          break;

        default:
          if (window.location.hash !== '#/home') {
            window.location.hash = '/home';
          } else {
            main.edit.init();
          }

          break;
      }
    }
  };
} (jQuery, this, this.document));
