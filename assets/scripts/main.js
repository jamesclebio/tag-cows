;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.alert = {
    settings: {
      autoinit: false,
      main: '.alert',
      template: '<div class="alert"></div>',
      speed: 100,
      timeout: 4000
    },

    builder: function (type, heading, message) {
      var that = this;
      var $main = $(this.settings.main);

      if ($main.length) {
        $main.remove();
      }

      $main = $(this.settings.template).appendTo($('body'));

      if (type) {
        $main.addClass(type);
      }

      if (heading) {
        $main.append('<h4>' + heading + '</h4>');
      }

      if (message) {
        $main.append('<p>' + message + '</p>');
      }

      $main.fadeIn(this.settings.speed, function () {
        $(this)
          .delay(that.settings.timeout)
          .fadeOut(that.settings.speed, function () {
            $(this).remove();
          });
      });
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.base = {
    settings: {
      autoinit: false,
      container: '.default-content > .container'
    },

    views: {
      edit: 'views/edit.html',
      list: 'views/list.html'
    },

    api: {
      url: 'http://recrutamento.taginterativa.com.br/api/v1/',
      accessToken: '4073b7d950'
    }
  };
} (jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.edit = {
    settings: {
      autoinit: false
    },

    init: function (update) {
      this.handler(update);
      this.builder(update);
    },

    handler: function (update) {
      var that = this;

      $(main.base.settings.container).on({
        submit: function(event) {
          var $fields = that.formFields();
          var fields = {};

          for (var i in $fields) {
            fields[i] = $fields[i].val();
          }

          if (that.formValidate($fields)) {
            if (update) {
              that.formSubmit(update, fields);
            } else {
              that.formSubmit(false, fields);
            }
          }

          event.preventDefault();
        }
      }, 'form');
    },

    builder: function (update) {
      var that = this;

      $.ajax({
        url: main.base.views.edit,

        beforeSend: function () {
          main.loading.on();
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          $(main.base.settings.container).html(data);

          if (update) {
            that.formFill(update);
          } else {
            main.loading.off();
          }

          $(':input:first').focus();

          if (window.location.hash === '#/home') {
            main.roll.to();
          } else {
            main.roll.to($(main.base.settings.container));
          }
        }
      });
    },

    formSubmit: function (update, fields) {
      var method = 'POST';
      var url = main.base.api.url + 'cows';

      if (update) {
        method = 'PUT';
        url += '/' + update;
      }

      $.ajax({
        method: method,
        url: url,
        contentType: 'application/json',
        headers: {
          'access-token': main.base.api.accessToken
        },
        data: JSON.stringify(fields),

        beforeSend: function() {
          main.loading.on();
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          if (data.error || data.errors) {
            this.error();
          } else {
            if (update) {
              main.alert.builder('success', 'Vaquinha atualizada com sucesso!');
            } else {
              main.alert.builder('success', 'Vaquinha cadastrada com sucesso!');
            }
            window.location.hash = '/list';
          }
        },

        timeout: 10000
      });
    },

    formFields: function () {
      return {
        weight: $('[name="weight"]'),
        age: $('[name="age"]'),
        price: $('[name="price"]')
      };
    },

    formFill: function (update) {
      var that = this;

      $.ajax({
        method: 'GET',
        url: main.base.api.url + 'cows/' + update,
        contentType: 'application/json',
        headers: {
          'access-token': main.base.api.accessToken
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          var $fields = that.formFields();

          for (var i in $fields) {
            $fields[i].val(data[i]);
          }

          $fields.price.val($fields.price.val().replace('.', ','));

          main.loading.off();
        },

        timeout: 10000
      });
    },

    formValidate: function ($fields) {
      if (!/\d{1,4}/.test($fields.weight.val())) {
        main.alert.builder('danger', 'O campo peso é obrigatório...', 'Deve conter somente números, com até 4 dígitos.');
        $fields.weight.focus();
        return false;
      }

      if (!/\d{1,2}/.test($fields.age.val())) {
        main.alert.builder('danger', 'O campo idade é obrigatório...', 'Deve conter somente números, com até 2 dígitos.');
        $fields.age.focus();
        return false;
      }

      if (!/\d+(,\d{2})?$/.test($fields.price.val())) {
        main.alert.builder('danger', 'O campo preço é obrigatório...', 'Deve conter um valor em R$ (Exemplo: 875,90).');
        $fields.price.focus();
        return false;
      }

      return true;
    }
  };
} (jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.list = {
    settings: {
      autoinit: false,
      rows: []
    },

    init: function () {
      this.handler();
      this.builder();
    },

    handler: function () {
      var that = this;
      var $container = $(main.base.settings.container);

      // Order trigger
      $container.on({
        click: function() {
          that.order($(this));
        }
      }, '[data-order]');

      // Action button (Update, Remove)
      $container.on({
        click: function(event) {
          var id = $(this).parents('tr').data('id');

          switch ($(this).data('action')) {
            case 'update':
              window.location.hash = '/edit/' + id;
              break;

            case 'remove':
              that.removeAction(id);
              break;
          }

          event.preventDefault();
        }
      }, '[data-action]');
    },

    builder: function () {
      var that = this;

      $.ajax({
        url: main.base.views.list,

        beforeSend: function () {
          main.loading.on();
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          $(main.base.settings.container).html(data);
          that.rows();
          main.roll.to();
        }
      });
    },

    rows: function (order, offset) {
      var that = this;

      offset = offset || 0;
      order = order || 'order_by[id]=DESC';

      $.ajax({
        method: 'GET',
        url: main.base.api.url + 'cows?offset=' + offset + '&' + order,
        contentType: 'application/json',
        headers: {
          'access-token': main.base.api.accessToken
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        beforeSend: function () {
          if (!offset) {
            that.settings.rows = [];
          }
        },

        success: function (data) {
          var $container = $('.table > tbody');
          var templateRow = $.templates('#templateRow');
          var recommended = 0;

          data = data || [];

          if (data.error ) {
            this.error();
          } else {
            that.settings.rows = that.settings.rows.concat(data);

            if (data.length === (offset + 20)) {
              that.rows(order, (offset + 20));
            } else {
              if (that.settings.rows.length) {
                for (var i in that.settings.rows) {
                  that.settings.rows[i].price = that.settings.rows[i].price.toFixed(2).replace('.', ',');
                  that.settings.rows[i].monthlyFood = that.monthlyFood(that.settings.rows[i].weight);
                  that.settings.rows[i].annualCost = that.annualCost(that.settings.rows[i].monthlyFood);
                  that.settings.rows[i].recommended = that.recommended(that.settings.rows[i]);

                  if (that.settings.rows[i].recommended > that.settings.rows[recommended].recommended) {
                    recommended = i;
                  }
                }

                that.settings.rows[recommended].recommendedClass = 'recommended';

                $container.html(templateRow.render(that.settings.rows));
                main.loading.off();
              } else {
                window.location.hash = '/';
              }
            }
          }
        },

        timeout: 10000
      });
    },

    monthlyFood: function (weight) {
      return (Math.round((((weight / 100) * 3) * 30) * 10) / 10).toString().replace('.', ',');
    },

    annualCost: function (monthlyFood) {
      return (((parseFloat(monthlyFood) / 30) * 365) * 0.2).toFixed(2).replace('.', ',');
    },

    recommended: function (item) {
      return (20 - item.age) / (parseFloat(item.annualCost) * parseFloat(item.price));
    },

    removeAction: function (id) {
      var that = this;

      $.ajax({
        method: 'DELETE',
        url: main.base.api.url + 'cows/' + id,
        contentType: 'application/json',
        headers: {
          'access-token': main.base.api.accessToken
        },

        beforeSend: function() {
          main.loading.on();
        },

        error: function () {
          main.alert.builder('danger', 'Ops! Algo deu errado... :(');
          main.loading.off();
        },

        success: function (data) {
          if (data.error) {
            this.error();
          } else {
            main.alert.builder('success', 'Vaquinha removida com sucesso!');
            that.rows();
          }
        },

        timeout: 10000
      });
    },

    order: function ($this) {
      var order = 'order_by' + $this.data('order');

      if (/ASC$/.test($this.data('order'))) {
        $this.data('order', $this.data('order').replace('=ASC', '=DESC'));
      } else {
        $this.data('order', $this.data('order').replace('=DESC', '=ASC'));
      }

      this.rows(order);
    }
  };
} (jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.loading = {
    settings: {
      autoinit: false,
      main: '.loading',
      template: '<div class="loading"><div class="container"><div class="spinner"></div></div></div>',
      speed: 100
    },

    builder: function () {
      var $main = $(this.settings.main);

      return ($main.length) ? $main : $(this.settings.template).appendTo($('body'));
    },

    on: function () {
      this
        .builder()
        .fadeIn(this.settings.speed);
    },

    off: function () {
      this
        .builder()
        .fadeOut(this.settings.speed);
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.roll = {
    settings: {
      autoinit: false,
      speed: 100
    },

    to: function ($target) {
      var that = this;

      if (!$target) {
        $target = $('body');
      }

      $('html, body').animate({
        scrollTop: $target.offset().top
      }, that.settings.speed);
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.route = {
    settings: {
      autoinit: true
    },

    init: function () {
      this.handler();
      this.view();
    },

    handler: function () {
      var that = this;

      $(window).on({
        hashchange: function() {
          that.view();
        }
      });
    },

    view: function () {
      switch (true) {
        case (/^#\/edit\/\d+/.test(window.location.hash)):
          main.edit.init(window.location.hash.replace(/^#\/edit\/(\d+)/, '$1'));
          break;

        case (/^#\/list/.test(window.location.hash)):
          main.list.init();
          break;

        default:
          if (window.location.hash !== '#/home') {
            window.location.hash = '/home';
          } else {
            main.edit.init();
          }

          break;
      }
    }
  };
} (jQuery, this, this.document));

;(function($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.init = function () {
    var resources = [];

    if (arguments.length) {
      resources = arguments;
    } else {
      for (var i in this) {
        resources.push(i);
      }
    }

    this.initTrigger(this, resources);
  };

  main.initTrigger = function (object, resources) {
    if (!resources) {
      resources = [];

      for (var i in object) {
        resources.push(i);
      }
    }

    for (var j in resources) {
      if (object[resources[j]].hasOwnProperty('settings') && object[resources[j]].settings.autoinit) {
        object[resources[j]].init();
      }
    }
  };

  main.init();
}(jQuery, this, this.document));
