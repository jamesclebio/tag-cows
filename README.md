# tag-cows (Teste da vaca)

Aplicação SPA desenvolvida para **Tag Interativa**.

## Ambiente

### Requisitos

* [Node.js](http://nodejs.org/)
* [npm](https://www.npmjs.org/)
* [gulp](http://gulpjs.com/)
* [Bower](http://bower.io/)
* [Sass](http://sass-lang.com/)
* [Compass](http://compass-style.org/)

### Instalação

Instale as dependências *gulp*:

`npm i`

Instale as dependências *Bower*:

`bower i`

## Rodando a aplicação

### Versão de produção (dist):

`gulp start:build`

Acesse **[http://localhost:3001](http://localhost:3001)** para testar a aplicação.

### Versão de desenvolvimento (source):

`gulp start:dev`

Acesse **[http://localhost:3000](http://localhost:3001)** para testar a aplicação.
