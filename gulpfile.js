var cleanCss = require('gulp-clean-css');
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var del = require('del');
var filter = require('gulp-filter');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var plumber = require('gulp-plumber');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
var runSequence = require('run-sequence');
var taskListing = require('gulp-task-listing');
var uglify = require('gulp-uglify');
var useref = require('gulp-useref');

// Default
gulp.task('default', taskListing);

// Start
gulp.task('start:dev', ['connect:dev', 'watch:dev']);
gulp.task('start:build', ['connect:build']);

// Connect
gulp.task('connect:dev', function () {
  connect.server({
    port: 3000,
    livereload: true
  });
});

gulp.task('connect:build', function () {
  connect.server({
    root: 'dist',
    port: 3001
  });
});

// Watch
gulp.task('watch:dev', function () {
  gulp.watch('scripts/**/*', ['scripts']);
  gulp.watch('styles/**/*', ['styles']);
  gulp.watch('**/*.html', ['reload']);
});

// Styles
gulp.task('styles', function () {
  return del('assets/styles/*', function () {
    gulp
      .src('styles/**/*.sass')
      .pipe(plumber())
      .pipe(compass({
        style: 'expanded',
        sass: 'styles',
        css: 'assets/styles',
        font: 'assets/fonts',
        image: 'assets/images'
      }))
      .pipe(gulp.dest('assets/styles'))
      .pipe(connect.reload());
  });
});

// Scripts
gulp.task('scripts', function () {
  return del('assets/scripts/*', function () {
    gulp
      .src([
        'scripts/main/main.*.js',
        'scripts/main/main.js'
      ])
      .pipe(plumber())
      .pipe(jshint())
      .pipe(jshint.reporter('default'))
      .pipe(concat('main.js'))
      .pipe(gulp.dest('assets/scripts'))
      .pipe(connect.reload());
  });
});

// Reload
gulp.task('reload', function () {
  gulp
    .src('**/*.html')
    .pipe(connect.reload());
});

// Build
gulp.task('build', function () {
  runSequence('build:clean', ['build:fonts', 'build:images'], 'build:mount', 'build:trim');
});

gulp.task('build:clean', function () {
  return del.sync('dist');
});

gulp.task('build:fonts', function () {
  return gulp
    .src('assets/fonts/**/*')
    .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('build:images', function () {
  return gulp
    .src('assets/images/**/*')
    .pipe(imagemin({
      optimizationLevel: 5
    }))
    .pipe(gulp.dest('dist/assets/images'));
});

gulp.task('build:mount', function() {
  var files = {
    source: [
      '*views/**/*',
      'index.html',
      'favicon.ico'
    ],

    views: filter([
      '**/*.html'
    ], {restore: true})
  };

  return gulp
    .src(files.source)

    // Views
    .pipe(files.views)
    .pipe(useref())
    .pipe(gulpif(
      '*.css',
      cleanCss({
        keepSpecialComments: 0
      })
    ))
    .pipe(gulpif(
      '*.js',
      uglify({
        mangle: false
      })
    ))
    .pipe(gulpif(
      '*.html',
      htmlmin({
        collapseWhitespace: true
      })
    ))
    .pipe(gulpif(
      '!index.html',
      rev()
    ))
    .pipe(files.views.restore)

    // Finishing
    .pipe(revReplace())
    .pipe(gulp.dest('dist'));
});

gulp.task('build:trim', function () {
  return del([
    'dist/assets/scripts/main.js',
    'dist/assets/styles/main.css'
  ]);
});
